<?php

namespace Tests;

use Src\helpers\Helpers;
use PHPUnit\Framework\TestCase;
use Src\controllers\Booking;

class BookingTest extends TestCase {

	private $booking;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->booking = new Booking();
	}

	/** @test */
	public function getBookings() {
		$results = $this->booking->getBookings();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['clientid'], 1);
		$this->assertEquals($results[0]['price'], 200);
		$this->assertEquals($results[0]['checkindate'], '2021-08-04 15:00:00');
		$this->assertEquals($results[0]['checkoutdate'], '2021-08-11 15:00:00');
	}

	/**
	 * @test
	 * @throws \HttpException
	 */
	public function createBookingWithNewClient() {
		$data = [
			'client' => [
				'username' => 'newuserwithbooking',
				'name' => 'New User Booking',
				'email' => 'newuserwithbooking@somewhere.com',
				'phone' => '1234566666'
			],
			'booking' => [
		        'price' => 200,
		        "checkindate" => "2023-12-04 15:00:00",
		        "checkoutdate" => "2023-12-11 15:00:00"
			]
		];

		Helpers::saveJson('bookings');
		Helpers::saveJson('clients');

		$result = $this->booking->createBookingWithNewClient($data);

		Helpers::restoreJson('bookings');
		Helpers::restoreJson('bookings');

		$this->assertEquals($result['client']['id'], 5);
		$this->assertEquals($result['client']['username'], 'newuserwithbooking');
		$this->assertEquals($result['client']['name'], 'New User Booking');
		$this->assertEquals($result['client']['email'], 'newuserwithbooking@somewhere.com');
		$this->assertEquals($result['client']['phone'], '1234566666');
		$this->assertEquals($result['booking']['id'], 3);
		$this->assertEquals($result['booking']['clientid'], 5);
		$this->assertEquals($result['booking']['price'], 200);
		$this->assertEquals($result['booking']['checkindate'], '2023-12-04 15:00:00');
		$this->assertEquals($result['booking']['checkoutdate'], '2023-12-11 15:00:00');
	}
}
