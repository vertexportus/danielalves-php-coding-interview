<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Client;
use Src\exceptions\ValidationException;
use Src\helpers\Helpers;

class ClientTest extends TestCase {

	private $client;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->client = new Client();
	}

	/** @test */
	public function getClients() {
		$results = $this->client->getClients();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['username'], 'arojas');
		$this->assertEquals($results[0]['name'], 'Antonio Rojas');
		$this->assertEquals($results[0]['email'], 'arojas@dogeplace.com');
		$this->assertEquals($results[0]['phone'], '1234567');
	}

	/** @test */
	public function createClient() {
		$client = [
			'username' => 'newuser2',
			'name' => 'New User 2',
			'email' => 'newuser2@dogeplace.com',
			'phone' => '5555555'
		];

		Helpers::saveJson('clients');

		$this->client->createClient($client);
		$results = $this->client->getClients();

		Helpers::restoreJson('clients');

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
	}

	/** @test */
	public function createClientEmailExists() {
		$client = [
			'username' => 'newuser',
			'name' => 'New User',
			'email' => 'arojas@dogeplace.com',
			'phone' => '5555555'
		];

		Helpers::saveJson('clients');

		$this->expectException(ValidationException::class);
		$this->client->createClient($client);

		Helpers::restoreJson('clients');
	}

	/** @test */
	public function updateClient() {
		$client = [
			'id' => 3,
			'username' => 'cperez',
			'name' => 'Carlos Perez',
			'email' => 'cperez@dogeplace.com',
			'phone' => '2222222'
		];

		Helpers::saveJson('clients');

		$this->client->updateClient($client);
		$results = $this->client->getClients();

		Helpers::restoreJson('clients');

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
	}
}
