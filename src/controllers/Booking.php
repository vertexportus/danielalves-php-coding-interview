<?php

namespace Src\controllers;

use Src\models\ClientModel;
use Src\models\BookingModel;
use Src\exceptions\ValidationException;

class Booking {

	private function getBookingModel(): BookingModel {
		return new BookingModel();
	}

	private function getClientModel() : ClientModel {
		return new ClientModel();
	}

	public function getBookings() {
		return $this->getBookingModel()->getBookings();
	}

	/**
	 * @throws \HttpException
	 */
	public function createBookingWithNewClient($data)
	{
		$newClientData = $data['client'];
		$newBookingData = $data['booking'];

		try {
			$client = $this->getClientModel()->createClient($newClientData);
			$newBookingData['clientid'] = $client['id'];
			$booking = $this->getBookingModel()->createBooking($newBookingData);

			return [
				'client' => $client,
				'booking' => $booking
			];
		}
		catch (\Exception $ex)
		{
			throw new \HttpException($ex->getMessage(), 400);
		}
	}
}
