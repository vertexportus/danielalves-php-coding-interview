<?php

namespace Src\models;

use Src\helpers\Helpers;
use Src\exceptions\ValidationException;

class ClientModel {

	private $clientData;

	function __construct() {
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/clients.json');
		$this->clientData = json_decode($string, true);
	}

	public function getClients() {
		return $this->clientData;
	}

	/**
	 * @throws ValidationException
	 */
	public function createClient($data) : array {
		$clients = $this->getClients();

		$checkEmailFilter = array_filter($clients, fn($c) => $c['email'] === $data['email']);
		if (count($checkEmailFilter) > 0) {
			throw new ValidationException("Account with supplied email already exists.");
		}

		$data['id'] = end($clients)['id'] + 1;
		$clients[] = $data;

		Helpers::putJson($clients, 'clients');

		return $data;
	}

	public function updateClient($data) : array {
		$updateClient = [];
		$clients = $this->getClients();
		foreach ($clients as $key => $client) {
			if ($client['id'] == $data['id']) {
				$clients[$key] = $updateClient = array_merge($client, $data);
			}
		}

		Helpers::putJson($clients, 'clients');

		return $updateClient;
	}

	public function getClientById($id) : ?array {
		$clients = $this->getClients();
		foreach ($clients as $client) {
			if ($client['id'] == $id) {
				return $client;
			}
		}
		return null;
	}
}
