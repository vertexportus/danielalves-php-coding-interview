<?php

namespace Src\helpers;

class Helpers {
	public static $savedData = [
		'clients' => null,
		'bookings' => null,
		'dogs' => null
	];
	public static function saveJson($entity) {
		static::$savedData[$entity] = file_get_contents(static::getEntityFilename($entity));
	}
	public static function restoreJson($entity) {
		if (static::$savedData[$entity] !== null) {
			file_put_contents(static::getEntityFilename($entity), static::$savedData[$entity]);
		}
	}
	public static function putJson($users, $entity) {
		file_put_contents(static::getEntityFilename($entity), json_encode($users, JSON_PRETTY_PRINT));
	}
	public static function arraySearchI($needle, $haystack, $column) {
		return array_search($needle, array_column($haystack, $column));
	}

	protected static function getEntityFilename($entity) {
		return dirname(__DIR__) . '/../scripts/'.$entity.'.json';
	}
}
